package cz.uhk.fim.rssreader.model;

public class RSSSources {
    private String name, source;

    public RSSSources(String name, String source) {
        this.name = name;
        this.source = source;
    }

    public String getName() {
        return name;
    }

    public RSSSources setName(String name) {
        this.name = name;
        return this;
    }

    public String getSource() {
        return source;
    }

    public RSSSources setSource(String source) {
        this.source = source;
        return this;
    }

    @Override
    public String toString() {
        return name;
    }
}

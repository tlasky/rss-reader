package cz.uhk.fim.rssreader.model;

import java.awt.*;

public class RSSItem {
    private String title;
    private String link;
    private String description;
    private String pubDate;
    private String author;

    public RSSItem() {}

    public RSSItem(String title, String link, String description, String pubDate, String author) {
        this.title = title;
        this.link = link;
        this.description = description;
        this.pubDate = pubDate;
        this.author = author;
    }

    public Color getColor() {
        return new Color(Math.abs(0xffffff - (title.length() + description.length()) * 1000));
    }

    public String getTitle() {
        return title;
    }

    public RSSItem setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getLink() {
        return link;
    }

    public RSSItem setLink(String link) {
        this.link = link;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public RSSItem setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getPubDate() {
        return pubDate;
    }

    public RSSItem setPubDate(String pubDate) {
        this.pubDate = pubDate;
        return this;
    }

    public String getAuthor() {
        return author;
    }

    public RSSItem setAuthor(String author) {
        this.author = author;
        return this;
    }
}

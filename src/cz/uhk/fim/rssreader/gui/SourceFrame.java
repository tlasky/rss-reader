package cz.uhk.fim.rssreader.gui;

import cz.uhk.fim.rssreader.model.RSSSources;
import cz.uhk.fim.rssreader.utils.FileUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.List;

public class SourceFrame extends JFrame {
    private JLabel lblName = new JLabel("Název:");
    private JLabel lblSource = new JLabel("Zdroj:");

    private JTextField txtName = new JTextField();
    private JTextField txtSource = new JTextField();

    private JPanel panel = new JPanel();

    private JButton btnOk = new JButton("OK");
    private JButton btnCancel = new JButton("Cancel");

    public SourceFrame() throws HeadlessException {
        initSelf();
    }

    private void initSelf() {
        setSize(200, 200);

        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        panel.add(lblName);
        panel.add(txtName);
        panel.add(lblSource);
        panel.add(txtSource);

        add(panel);

        panel.add(btnOk, BorderLayout.WEST);
        panel.add(btnCancel, BorderLayout.EAST);

        btnCancel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                dispose();
            }
        });
    }

    public void addInit() {
        btnOk.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                try {
                    List<RSSSources> sources = FileUtils.loadSources();
                    sources.add(new RSSSources(txtName.getText(), txtSource.getText()));
                    FileUtils.saveSources(sources);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });
    }

    public void editInit(RSSSources source) {
        btnOk.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                try {
                    List<RSSSources> sources = FileUtils.loadSources();
                    RSSSources originalSrc = null;

                    for (RSSSources src : sources) {
                        if (src.getSource().equals(source.getSource()) && src.getName().equals(source.getName())) {
                            originalSrc = src;
                            break;
                        }
                    }

                    if (originalSrc != null) {
                        originalSrc.setName(txtName.getText());
                        originalSrc.setSource(txtSource.getText());
                    }

                    FileUtils.saveSources(sources);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });
    }

    public void setSourceName(String text) {
        txtName.setText(text);
    }

    public void setSourceText(String text) {
        txtSource.setText(text);
    }
}

package cz.uhk.fim.rssreader.gui;

import cz.uhk.fim.rssreader.model.RSSItem;
import cz.uhk.fim.rssreader.model.RSSList;
import cz.uhk.fim.rssreader.model.RSSSources;
import cz.uhk.fim.rssreader.utils.FileUtils;
import cz.uhk.fim.rssreader.utils.RSSParser;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.nio.file.NoSuchFileException;
import java.util.ArrayList;
import java.util.List;

public class MainFrame extends JFrame {
    private JLabel lblError = new JLabel();
    private RSSList list;
    private DetailFrame detail;

    private  JPanel content;
    private JComboBox<RSSSources> cbSources;

    private JButton btnAdd;
    private JButton btnEdit;
    private JButton btnLoad;
    private JButton btnDelete;

    private List<RSSSources> sources;


    public MainFrame() {
        init();
    }

    public void init() {
        setTitle("RSS Reader");
        setSize(500, 500);
        setVisible(true);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        initComponents();
    }

    public boolean validatePath(String text) {
        boolean empty = text.trim().isEmpty();
        if (empty) {
            showErrorMessage("Chybí vstup!");
        } else {
            hideErrorMessage();
        }
        return !empty;
    }

    private void showErrorMessage(String message) {
        lblError.setVisible(true);
        lblError.setText(message);
    }

    private void hideErrorMessage() {
        lblError.setVisible(false);
    }

    public void initComponents() {
        JPanel controlPanel = new JPanel(new BorderLayout());
        JPanel sourceControlPanel = new JPanel(new FlowLayout());

        btnAdd = new JButton("Add");
        sourceControlPanel.add(btnAdd);

        btnEdit = new JButton("Edit");
        sourceControlPanel.add(btnEdit);

        btnLoad = new JButton("Load");
        sourceControlPanel.add(btnLoad);

        btnDelete = new JButton("Delete");
        sourceControlPanel.add(btnDelete);

        cbSources = new JComboBox<>();

        lblError.setVisible(false);
        lblError.setForeground(Color.RED);
        lblError.setHorizontalAlignment(SwingConstants.CENTER);


        controlPanel.add(cbSources, BorderLayout.NORTH);
        controlPanel.add(sourceControlPanel, BorderLayout.CENTER); // Buttony pro ty zdroje
        controlPanel.add(lblError, BorderLayout.SOUTH);

        content = new JPanel(new WrapLayout());
        add(controlPanel, BorderLayout.NORTH);
        add(new JScrollPane(content), BorderLayout.CENTER);

        initSources();

        btnLoad.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                RSSSources source = (RSSSources) cbSources.getSelectedItem();
                loadRSS(source.getSource());
                revalidate();
            }
        });

        /*
            Co už... 🤷‍
            Btw. Jak jinak bych dostal instanci MainFramu do toho adaptéru?
        */
        MainFrame thisFrame = this;

        btnAdd.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                SourceFrame addSource = new SourceFrame(thisFrame, sources);
                addSource.addInit();
                addSource.show();
            }
        });

        btnEdit.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                SourceFrame editSource = new SourceFrame(thisFrame, sources);
                editSource.editInit((RSSSources) cbSources.getSelectedItem());
                editSource.show();
            }
        });

        btnDelete.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                RSSSources source = (RSSSources) cbSources.getSelectedItem();
                sources.remove(source);
                FileUtils.saveSources(sources);
                initSources();
            }
        });
    }

    public void initSources() {
        try {
            sources = FileUtils.loadSources();
            cbSources.removeAllItems();
            for (RSSSources source : sources) {
                cbSources.addItem(source);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadRSS(String path) {
        try {
            if (validatePath(path)) {
                list = new RSSParser().getParsedRSS(path);
                for (RSSItem item:list.getItems()) {
                    JPanel card = new CardView(item);
                    content.add(card);

                    card.addMouseListener(new MouseAdapter() {
                        @Override
                        public void mouseClicked(MouseEvent e) {
                            detail = new DetailFrame(item);
                            detail.show();
                        }
                    });
                }
            }
        } catch (NoSuchFileException exc) {
            showErrorMessage(String.format("Soubor \"%s\" neexistuje.", path));
        } catch (Exception exc) {
            exc.printStackTrace();
            showErrorMessage(exc.toString());
        }
    }
}
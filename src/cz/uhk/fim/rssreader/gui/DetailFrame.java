package cz.uhk.fim.rssreader.gui;

import cz.uhk.fim.rssreader.model.RSSItem;
import cz.uhk.fim.rssreader.utils.FileUtils;
import cz.uhk.fim.rssreader.utils.Helpers;
import javafx.scene.layout.BorderRepeat;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;

public class DetailFrame extends JFrame {
    RSSItem item;

    JLabel txtTitle = new JLabel();
    JLabel txtDescription = new JLabel();
    JLabel txtAdditional = new JLabel();
    JPanel bottomPanel = new JPanel();
    JButton btnSave = new JButton("Uložit jako text.");
    JPanel panel = new JPanel(new BorderLayout());

    public DetailFrame(RSSItem item) throws HeadlessException {
        this.item = item;
        setTitle(item.getTitle());
        setSize(500, 500);
        setUndecorated(true);
        setLayout(new BorderLayout());

        panel.setSize(getSize());
        panel.setBackground(item.getColor());
        add(panel);

        txtTitle.setSize(panel.getWidth(), 1);
        txtTitle.setFont(new Font("Courier", Font.BOLD,  12));
        txtTitle.setText(item.getTitle());

        txtDescription.setVerticalAlignment(SwingConstants.TOP);
        txtDescription.setSize(panel.getWidth(), 1);
        txtDescription.setFont(new Font("Courier", Font.PLAIN,  11));
        txtDescription.setText(Helpers.contentFormat(item.getDescription(), txtDescription.getWidth() - 200));

        txtAdditional.setText(String.format(
                "Přidáno: %s    Autor: %s",
                item.getPubDate(),
                item.getAuthor() != null ? item.getAuthor()  : "unknown"
        ));

        panel.add(txtTitle, BorderLayout.NORTH);
        panel.add(new JScrollPane(txtDescription), BorderLayout.CENTER);
        bottomPanel.add(txtAdditional, BorderLayout.NORTH);
        bottomPanel.add(btnSave, BorderLayout.NORTH);
        panel.add(bottomPanel, BorderLayout.SOUTH);


        txtDescription.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (SwingUtilities.isRightMouseButton(e)) {
                    dispose();
                }
            }
        });

        initListeners();
    }

    private void initListeners() {
        btnSave.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                try {
                    BufferedWriter writer = new BufferedWriter(new FileWriter(Helpers.filenameFromTitle(item.getTitle())));
                    writer.write(item.getDescription());
                    writer.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });
    }
}

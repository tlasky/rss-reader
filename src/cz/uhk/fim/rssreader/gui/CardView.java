package cz.uhk.fim.rssreader.gui;

import cz.uhk.fim.rssreader.model.RSSItem;
import cz.uhk.fim.rssreader.utils.Helpers;

import javax.swing.*;
import java.awt.*;

public class CardView extends JPanel {
    public static final int ITEMM_WIDTH = 180;
    public static final int COMPONENT_WITH = 160;
    public static final int HEIGHT = 1;

    public  static final int CONTENT_MAX_LENTH = 150;


    public CardView(RSSItem item) {
        setLayout(new WrapLayout());
        setSize(ITEMM_WIDTH, HEIGHT);

        setTitle(item.getTitle());
        setDescription(item.getDescription());
        setAdditional(item.getPubDate(), item.getAuthor());

        setBackground(item.getColor());
    }

    private void setTitle(String title) {
        JLabel lblTitle = new JLabel();
        lblTitle.setSize(COMPONENT_WITH, HEIGHT);
        lblTitle.setFont(new Font("Courier", Font.BOLD,  12));

        add(lblTitle);

        lblTitle.setText(Helpers.contentFormat(title, COMPONENT_WITH));
    }

    private void setDescription(String description) {
        JLabel lblDescription = new JLabel();
        lblDescription.setSize(COMPONENT_WITH, HEIGHT);
        lblDescription.setFont(new Font("Courier", Font.PLAIN,  11));

        add(lblDescription);

        lblDescription.setText(Helpers.contentFormat(Helpers.contentLegth(description, CONTENT_MAX_LENTH), COMPONENT_WITH));
    }

    private void setAdditional(String pubDate, String author) {
        JLabel lblAdditional = new JLabel();
        lblAdditional.setSize(COMPONENT_WITH, HEIGHT);
        lblAdditional.setFont(new Font("Courier", Font.ITALIC,  10));
        lblAdditional.setForeground(Color.lightGray);

        add(lblAdditional);

        lblAdditional.setText(Helpers.contentFormat(String.format(
                "%s\n%s",
                pubDate,
                author != null ? author : ""
        ), COMPONENT_WITH));
    }
}

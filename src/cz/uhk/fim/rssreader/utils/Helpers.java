package cz.uhk.fim.rssreader.utils;

import com.sun.deploy.util.StringUtils;
import cz.uhk.fim.rssreader.model.RSSItem;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Helpers {
    public static String contentLegth(String content, int maxLenth) {
        if (content.length() >= maxLenth) {
            content = content.substring(0, maxLenth - 3) + "...";
        }
        return content;
    }

    public static String contentFormat(String content, int width) {
        return String.format(
                "<html><p style=\"width: %dpx;\">%s</p></html>",
                width,
                content
        );
    }

    public static String filenameFromTitle(String title) {
        List<String> strings = Arrays.asList(title.split(" "));
        return StringUtils.join(strings.subList(0, 3), "_") + ".txt";
    }
}

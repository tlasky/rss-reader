package cz.uhk.fim.rssreader.utils;

import cz.uhk.fim.rssreader.model.RSSList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.net.URL;

public class RSSParser {
    private RSSList items;
    private ItemHandler handler;

    public RSSParser() {
        items = new RSSList();
        handler = new ItemHandler(items);
    }

    private void parse(String source) throws ParserConfigurationException, SAXException, IOException {
        SAXParserFactory factory = SAXParserFactory.newInstance(); // Singleton
        SAXParser parser = factory.newSAXParser();

        if (source.startsWith("http")) {
            parser.parse(new InputSource(new URL(source).openStream()), handler); // Otevírání adres
        } else {
            parser.parse(new File(source), handler);
        }
    }

    public RSSList getParsedRSS(String source) throws IOException, SAXException, ParserConfigurationException {
        parse(source);
        return items;
    }
}

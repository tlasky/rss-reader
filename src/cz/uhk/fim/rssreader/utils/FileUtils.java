package cz.uhk.fim.rssreader.utils;

import cz.uhk.fim.rssreader.model.RSSSources;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class FileUtils {
    public static final String CONFIG_FILE = "./config.cfg";

    public static String readStringFromFile(String filePath) throws IOException {
        return new String(Files.readAllBytes(Paths.get(filePath)));
    }

    public static void writeStringToFile(String filePath, String content) throws IOException {
        Files.write(Paths.get(filePath), content.getBytes(Charset.forName("UTF-8")));
    }

    public static List<RSSSources> loadSources() throws IOException {
        List<RSSSources> sources = new ArrayList<>();
        new BufferedReader(new StringReader(FileUtils.readStringFromFile(CONFIG_FILE))).lines().forEach(
                source -> {
                    String[] parts = source.split(";");
                    sources.add(new RSSSources(parts[0], parts[1]));
                }
        );
        return sources;
    }

    public static void  saveSources(List<RSSSources> sources) {
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < sources.size(); i++) {
            builder.append(String.format(
                    "%s;%s",
                    sources.get(i).getName(),
                    sources.get(i).getSource()
            ));
            builder.append(i != sources.size() ? "\n" : "");
        }

        try {
            writeStringToFile(CONFIG_FILE, builder.toString());
        } catch (Exception e) {

        }
    }
}

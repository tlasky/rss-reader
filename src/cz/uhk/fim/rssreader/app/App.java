package cz.uhk.fim.rssreader.app;

import cz.uhk.fim.rssreader.gui.MainFrame;

import javax.swing.*;


/*
* http://servis.idnes.cz/rss.aspx?c=hradec
* */


public class App {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new MainFrame();
            }
        });
    }
}
